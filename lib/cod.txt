window.ondragover = function(e) {e.preventDefault()}
		window.ondrop = function(e) {e.preventDefault(); draw(e.dataTransfer.files[0]); }
		    
		function draw(file){
		    
		    var img =new Image();
		  
		   	img.src = (window.webkitURL ).createObjectURL(file);
		    var canvas=document.getElementById("cnvs");
		    var ctx = canvas.getContext("2d");
		  
		    img.onload = function() {
		    	
   				ctx.drawImage(img, 0, 0, img.width, img.height, 0, 0, canvas.width, canvas.height); // stretch img to canvas size
			}
        }